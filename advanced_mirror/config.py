class Config:
    regex = r"(.*)\.proxy\.(ygg|anon)"
    replace_regex = r"(https?:)?\/\/{host}\/"
    replace_regex_str = "/"
    replace_types = ["text/css", "text/html"]
    replace_https_to_http = True
    ipv6 = True
    ygg = True
    blocked_headers = [
        "content-encoding",
        "content-length",
        "transfer-encoding",
        "connection",
        "server",
        "strict-transport-security",
        "upgrade",
        "content-security-policy",
        "cross-origin-opener-policy",
        "permissions-policy",
    ]
