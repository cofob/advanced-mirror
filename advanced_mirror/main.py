import re

from aiohttp import web, ClientSession
from aiohttp.web import Request, Response
from bitarray import bitarray
from ipaddress import IPv6Address
from aiodns import DNSResolver
from advanced_mirror.config import Config
from re import findall
import asyncio


async def handle(request: Request) -> Response:
    path = request.path
    host = request.host

    regex = findall(Config.regex, host)
    try:
        regex = regex[0]
    except IndexError:
        return Response(text="Invalid domain", status=404)

    remote_host = regex[0] + "." + regex[1]

    remote_host_ip = await resolver.query(remote_host, "AAAA" if Config.ipv6 else "A")
    remote_host_ip = remote_host_ip[0].host

    if Config.ipv6:
        try:
            ipv6 = IPv6Address(remote_host_ip)
        except ValueError:
            return Response(text="Non IPv6", status=404)

        if Config.ygg:
            arr = bitarray()
            arr.frombytes(int(ipv6).to_bytes(16, "big"))
            if not arr[:8].tobytes() == b"\x02":
                if not arr[:8].tobytes() == b"\x03":
                    return Response(text="Non Ygg address", status=404)

    async with ClientSession() as session:
        if request.method == "GET":
            response = await session.get(("http" if Config.ygg else "https") + "://" + remote_host + path, ssl=False)
        elif request.method == "POST":
            response = await session.post(
                ("http" if Config.ygg else "https") + "://" + remote_host + path,
                ssl=False,
                data=await request.post(),
            )
        elif request.method == "PATCH":
            response = await session.patch(
                ("http" if Config.ygg else "https") + "://" + remote_host + path,
                ssl=False,
                data=await request.post(),
            )
        elif request.method == "OPTIONS":
            response = await session.options(
                ("http" if Config.ygg else "https") + "://" + remote_host + path, ssl=False
            )
        elif request.method == "HEAD":
            response = await session.head(("http" if Config.ygg else "https") + "://" + remote_host + path, ssl=False)
        elif request.method == "PUT":
            response = await session.put(
                ("http" if Config.ygg else "https") + "://" + remote_host + path,
                ssl=False,
                data=await request.post(),
            )

        text = await response.read()
        headers_raw = response.headers
        headers = {}
        for header in headers_raw:
            header_low = header.lower()
            if header_low not in Config.blocked_headers:
                headers[header] = headers_raw[header]
            if header_low == "content-type":
                val = headers_raw[header].lower()
                if val.split(";")[0] in Config.replace_types:
                    try:
                        text = text.decode()
                        text = re.sub(Config.replace_regex.format(host=remote_host), Config.replace_regex_str, text)
                        if Config.replace_https_to_http:
                            text = text.replace("https", "http")
                    except UnicodeDecodeError:
                        print("decode")

        response.close()

        return Response(body=text, headers=headers, status=response.status)


app = web.Application()
app.add_routes(
    [
        web.get(r"/{name:.*}", handle),
        web.post(r"/{name:.*}", handle),
        web.put(r"/{name:.*}", handle),
        web.options(r"/{name:.*}", handle),
        web.patch(r"/{name:.*}", handle),
        web.head(r"/{name:.*}", handle),
    ]
)

loop = asyncio.new_event_loop()
resolver = DNSResolver(loop=loop)

if __name__ == "__main__":
    web.run_app(app, loop=loop, host="127.0.0.1", port=8001)
